## Tools needed

* **minikube** - A local installation of Kubernetes for various machines unsupported .
* **kubectl** - Administer Kubernetes (minikube or full install).
* **Docker** - Tools for containerization.

## Start

* `minikube start` - Start the Minikube VM and initialize the local cluster.
* `kubectl cluster-info` - Check if kubectl detects your cluster.
* `minikube dashboard` - Open the Kubernetes dashboard in your browser.

## Deploy a Docker image
* `minikube docker-env` - Follow instructions. Configure Docker to use Minikube's Docker daemon.
* `docker build -t hello-node:v1 .` - Note the dot. Build the Docker image using Minikube's Docker daemon.

Container - an isolated OS environment for your app.
Pod - a group of containers.
Deployment - checks on the health of your pod.

* `kubectl run hello-node --image=hello-node:v1 --port=8080` - Create a deployment that manages a pod.

## Manage your cluster

* `kubectl get deployments` - View your deployments.
* `kubectl get pods` - View your pods.
* `kubectl get events` - View cluster events.
* `kubectl config view` - View current kubectl config.

## Expose the pod

Service - exposes the pod outside the cluster.

* `kubectl expose deployment hello-node --type=LoadBalancer` - Expose the pod to the public
* `minikube service hello-node` - Open the exposed pod in your browser.
* `kubectl logs hello-node-xxx-pod-name-xxxxx` - View pod logs.

Namespace - a grouping of Kubernetes objects.
* `minikube service list` - List all services in the Minikube.

## Update your app

* `docker build -t hello-node:v2 .` - Build a new Docker image.

Instead of `kubectl run`, we use:
* `kubectl set image deployment/hello-node hello-node=hello-node:v2` - Update the image of the deployment.
* `minikube service hello-node` - Open the exposed pod in your browser.

## Minikube addons 

* `minikube addons list`
* `minikube addons enable heapster` - Enable Heapster.
* `minikube get po,svc -n kube-system` - List pods and services.
* `minikube addons open heapster` - Open the Heapster GUI in your browser.

### Addons list

* kube-dns - Built-in DNS server. Maps domain names to every service.
* heapster - Container cluster monitoring and performance analysis.
* addon-manager
* default-storageclass
* coredns
* ingress - Nginx ingress controller.
* registry - Docker registry
* registry-creds
* dashboard - Kubernetes dashboard.
* storage-provisioner

## Useful environment variables (node.js)

* `NO_UPDATE_NOTIFIER` - Opt out of update notifications using [update-notifier](https://github.com/yeoman/update-notifier) (like npm).

## Using Helm

* **Helm** - CLI tool for installing Kubernetes charts. Similar to a package manager (npm, composer, etc.).
* _chart_ - A pre-configured process of installing deployments/etc.
* **Tiller** - An in-cluster deployment used to manage Kubernetes charts.

* `helm init` - Install Tiller on your Kubernetes cluster.
* `helm version` - Show client (Helm) and server (Tiller) version.

## Obtaining charts 

[KubeApps Hub](https://hub.kubeapps.com/) lists all officially-maintained Kubernetes charts.

Additional lists of charts:
* [Astrometrics](https://astrometrics.io/)
* [Fabric8](https://fabric8.io/helm/)

## Creating Kubernetes objects

* `kubectl create -f ./deployment.yaml` - Create a deployment.
* `kubectl apply -f ./deployment.yaml` - Update a deployment by metadata name.

## Editing Kubernetes objects

* `kubectl get ing` - Get specified objects
* `kubectl edit svc/service1` - Edit an existing object.
* `kubectl edit -f ./deployment.yaml` - Edit an object based on the YAML file.

## Implementing user-defined health checks

Reference: [Kubernetes 201](https://kubernetes.io/docs/user-guide/walkthrough/k8s201/)

* Run a web hook and check if its status code is 200-399. [Examples](https://kubernetes.io/docs/user-guide/liveness/)
* Run a command on the container and see if it returns an exit code of 0. [Examples](https://kubernetes.io/docs/user-guide/liveness/)
* Attempt to create a TCP socket to your container.

## Creating an Ingress

* **Ingress** - Manages incoming traffic.
* **Ingress controller** - A pod that satisfies the needs of the ingress configuration.

Reference: [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/)

* `minikube addons enable ingress` - Set up an nginx ingress controller.
* Use `kubectl create -f` to upload a YAML file of the ingress.
* `kubectl get ing` - Get all ingresses.
* `kubectl edit ing name-of-ingress` - Edit an ingress.

[Official docs for ingress-nginx](https://github.com/kubernetes/ingress-nginx/blob/master/README.md)

Example of ingress for name-based virtual hosting:
```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-ingress
spec:
  backend:
    serviceName: default-http-backend
    servicePort: 80
  rules:
  - host: hellonode.minikube.local
    http:
      paths:
      - backend:
          serviceName: hellonode
          servicePort: 8080
        path: /
  - host: henlonode.minikube.local
    http:
      paths:
      - backend:
          serviceName: henlonode
          servicePort: 8080
        path: /
```

### Configuring TLS (TLS termination)

* `openssl req -newkey rsa:2048 -nodes -keyout server.pem -x509 -days 365 -out server.csr` - Create a key and a CSR.
  * `openssl req -newkey rsa:2048 -nodes -keyout server.pem -x509 -days 365 -out server.csr -reqexts SAN -config server.cnf` - Create a key and a CSR with config.
  
Example of config with subjectAltName

```inf
[SAN]
subjectAltName=DNS:example.com,DNS:www.example.com
```

* `openssl pkcs12 -inkey server.pem -in server.csr -export -out server.p12` - Create a PKCS#12 package containing the key and the cert.
* `kubectl create secret tls ingress-secret --key server.pem --cert server.csr` - Create a TLS secret.
* Edit the ingress and set `spec.tls` to the name of the secret.
* `openssl x509 -req -in server.csr -signkey server.pem -out server.cer` - Self-sign the CSR into a certificate.

## Create a ConfigMap

Config maps allow settings to be decoupled from the container.

Example config map:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: special-config
  namespace: default
data:
  SPECIAL_LEVEL: very
  SPECIAL_TYPE: charm
```

Add all key-value pairs into a pod or deployment:

```yaml
spec: 
  containers: 
    -
      envFrom:
        - configMapRef:
            name: special-config
```

[ConfigMap documentation](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/)

## Windows port forwarding

[Portproxy reference](https://technet.microsoft.com/en-us/library/cc731068(v=ws.10).aspx)

* `netsh interface portproxy show all` - Show all rules.
* `netsh interface portproxy show v4tov4` - Show v4 to v4 rules.
* `netsh interface portproxy add v4tov4 listenport= [connectaddress=127.0.0.1] [connectport=<listenport>] [listenaddress=127.0.0.1]` - Forwards IPv4 packets to another IP address and/or port.
* `netsh interface portproxy delete v4tov4 listenport= [listenaddress=]` - Delete rule.

## Creating a persistent volume

Example PV in Minikube:
```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv1
spec:
  storageClassName: standard
  accessModes: 
    -
      ReadWriteOnce
  capacity:
    storage: 2Gi
  hostPath: 
    path: /data/pv1
```

## Deploy a MongoDB database using Helm

* `helm install --name mongodb1 --set persistence.size=2Gi stable/mongodb` - Deploy MongoDB. Requires `standard` storage class PV.
* `kubectl run mongodb1-mongodb-client --rm --tty -i --image bitnami/mongodb --command -- mongo --host mongodb1-mongodb` - Connect to your database using a temporary pod.
* Change the service type to NodePort or LoadBalancer to make it public.
* `minikube service mongodb1-mongodb` - Will open the exposed IP address / port. Get the host/port.
* `mongo mongodb://192.168.xx.xx:3xxxx` - Open a mongo shell to the database.
* `mongorestore /h 192.168.xx.xx:3xxxx` - Restore a database dump through the exposed host/port.

## Configure local network DNS

* Configure the router's DHCP settings to point to the host.
  * Refer to the port forwarding section for further port forwarding.
* Install the `coredns` chart.
* Configure [Custom DNS entries](https://coredns.io/2017/05/08/custom-dns-entries-for-kubernetes/).

## Configure liveness checks

Kubernetes checks if your app is still alive.

Define a liveness HTTP request:

```yaml
spec:
  containers: 
    - 
      livenessProbe: 
        httpGet:
          path: /healthz
          port: 8080
          httpHeaders:
            - name: X-Custom-Header
              value: awesome
        initialDelaySeconds: 3
        periodSeconds: 3
```

Make sure to implement the liveness check in your server.
