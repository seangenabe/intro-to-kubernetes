## Install KVM2 driver

sudo apt install libvirt-bin qemu-kvm
sudo usermod -a -G libvirtd $(whoami)
newgrp libvirtd
curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2 && \
  chmod +x docker-machine-driver-kvm2 && \
  sudo mv docker-machine-driver-kvm2 /usr/local/bin/


## Install kubectl

sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo touch /etc/apt/sources.list.d/kubernetes.list 
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get install -y kubectl

## Turn off error reports

minikube config set WantReportErrorPrompt false

## minikube-net

sudo virsh
net-start minikube-net
quit

## Install minukube

curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.28.0/minikube-linux-amd64 && \
  chmod +x minikube && \
  sudo mv minikube /usr/local/bin/